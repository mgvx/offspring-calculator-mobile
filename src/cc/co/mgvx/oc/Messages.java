/* Copyright © 2012 Marius Gavrilescu
 *
 * This file is part of Offspring Calculator.
 * Offspring Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Offspring Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Offspring Calculator.  If not, see <http://www.gnu.org/licenses/>.
 */
package cc.co.mgvx.oc;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Class to get strings from the main <code>ResourceBundle</code>
 * @author Marius Gavrilescu
 *
 */
class Messages {
	/** The name of {@link #RESOURCE_BUNDLE} */
	private static final String BUNDLE_NAME = "cc.co.mgvx.oc.messages"; //$NON-NLS-1$
	/** The <code>ResourceBundle</code>	we are using */
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private Messages() {
	}

	/**
	 * Get a string from the main <code>ResourceBundle</code>
	 * @param key the key to get
	 * @return the string
	 */
	public static String getString(final String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
