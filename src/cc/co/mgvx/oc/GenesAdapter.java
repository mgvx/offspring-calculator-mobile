/* Copyright © 2012 Marius Gavrilescu
 *
 * This file is part of Offspring Calculator.
 * Offspring Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Offspring Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Offspring Calculator.  If not, see <http://www.gnu.org/licenses/>.
 */
package cc.co.mgvx.oc;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

/**
 * A custom <code>ArrayAdapter&lt;Gene&gt;</code>
 * @author Marius Gavrilescu
 */
final class GenesAdapter extends ArrayAdapter<Gene> {
	/** The array of <code>Gene</code>s */
	private final Gene[] genes;
	/** The <code>Context</code> of this adapter */
	private final Context theContext;
	/**
	 * Only constructor for <code>GenesAdapter</code>
	 * @param context the <code>Context</code> of this adapter
	 * @param textViewResourceId the resourceID for a textview [only used in super]
	 * @param genes the array of <code>Gene</code>s
	 */
	public GenesAdapter(final Context context, final int textViewResourceId, final Gene[] genes) {
		super(context, textViewResourceId, genes);
		this.genes=genes;
		this.theContext=context;
		//setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	}
	@Override
	public View getView (final int position, final View convertView, final ViewGroup parent){
		TextView tv=null;
		if(convertView==null||!(convertView instanceof TextView)){
			//final LayoutInflater inflater=LayoutInflater.from(this.theContext);
			//tv=(TextView) inflater.inflate(android.R.layout.simple_spinner_item, null);
			tv=new TextView(this.theContext);
		}
		else
			tv=(TextView) convertView;
		if(this.genes[position]==null)
			tv.setText(Messages.getString("ga.skip")); //$NON-NLS-1$
		else
			tv.setText(Html.fromHtml(this.genes[position].toString()));
		tv.setTextColor(Color.BLACK);
		return tv;
	}
	@Override
	public View getDropDownView (int position, View convertView, ViewGroup parent){
		CheckedTextView ctv=null;
		if(convertView==null||!(convertView instanceof CheckedTextView)){
			final LayoutInflater inflater=LayoutInflater.from(this.theContext);
			ctv=(CheckedTextView) inflater.inflate(android.R.layout.select_dialog_singlechoice, null);
		}else
			ctv=(CheckedTextView) convertView;
		if(this.genes[position]==null)
			ctv.setText(Messages.getString("ga.skip")); //$NON-NLS-1$
		else
			ctv.setText(Html.fromHtml(this.genes[position].toString()));
		return ctv;
	}
}
