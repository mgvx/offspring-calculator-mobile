/* Copyright © 2012 Marius Gavrilescu
 *
 * This file is part of Offspring Calculator.
 * Offspring Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Offspring Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Offspring Calculator.  If not, see <http://www.gnu.org/licenses/>.
 */
package cc.co.mgvx.oc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;
import com.google.android.vending.licensing.ServerManagedPolicy;
/**
 * The main activity of the app
 * @author Marius Gavrilescu
 */
public final class MainActivity extends Activity {
	private final class MyLicenseCheckerCallback implements LicenseCheckerCallback{
		private int tries=0;
		@Override
		public void allow(int reason) {
			return;
		}

		@Override
		public void dontAllow(int reason) {
			this.tries++;
			if(reason==Policy.RETRY&&this.tries<10){//10 tries ought to be enough for anybody
				MainActivity.this.lc.checkAccess(MainActivity.this.lcc);
				return;
			}
			err();
		}

		@Override
		public void applicationError(final int errorCode) {
			MainActivity.this.handler.post(new Runnable() {
				@Override
				public void run() {
					String errStr=String.format(Messages.getString("MainActivity.lvl_app_error"), errorCode); //$NON-NLS-1$
					Toast.makeText(MainActivity.this,
							errStr,
							Toast.LENGTH_LONG).show();
					finish();
				}
			});
		}
	}
	/** The base64 public key used in licensing */
	private static final String BASE64_KEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtZHl6RiuG99Ac9Q+qQW757jY7x0Vps7IqUT01ebUEyfLKWTk8WPJvcvlbQeAAbHfJmHh77HnG8yBYadh8CwLVcHwh95BZoFSylz/UFxzRlpPj3WSOGsGxCKcPCKHjNsN9DoXLip3G2phM1OIGp5+DsxjeJZerjILsedZNzOm3uMJ2p81AeGnWeQ7k0ljqlzEZhmrfHEj3xEmQoCKlQwfxlOfSeLe+h5YMCNxflV6RzWpDR1QwaeFuKwGkFfl81Kq9MqKdh11ItBvGuC3XtZq8evykGLNLW7fvbM/CK0Uda7YNYRgfD0NHFQUdQPHrD+3rMktZ/5NCRZnP/jxfnORBQIDAQAB"; //$NON-NLS-1$
	/** Salt used in licensing */
	private static final byte[] SALT={
		-28,		-110,		+55,	-86,	 -79,
		 30,		 -73,		-26,	107,	   8,
		-46,		 -23,		+53,	+86,	 -85,
		 50,		 -95,		-42,	-44,	-108
	};
	/** Map from a <code>Gene.group</code> to spinners */
	private static final Map<CharSequence,Spinner> spinnerMap=new HashMap<CharSequence,Spinner>(30);
	/** If true, licensing is not checked */
	private static final boolean skipLicensing = true;
	/** The current target. Every <code>Gene</code> shown must target this */
	private static long currentTarget=Gene.TARGET_FEMALE;
	/**
	 * True if this is the first run of the activity
	 * 
	 * When true, 'skip' options appear.
	 * When false, skipped options don't appear
	 */
	private static boolean first=true;
	/** Map from a <code>Gene.group</code> to a <code>GeneSet</code> for this <code>Gene.group</code> */
	public static Map<CharSequence,GeneSet> geneSetMap=new HashMap<CharSequence,GeneSet>(30);
	/** Handler used in <code>MyLicenseCheckerCallback</code> */
	private Handler handler;
	/** The <code>MyLicenseCheckerCallback</code> used */
	private LicenseCheckerCallback lcc;
	/** The <code>LicenseChecker</code> used */
	private LicenseChecker lc;
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		if(Build.VERSION.SDK_INT<11)
			return super.onCreateOptionsMenu(menu);
		final MenuInflater inf=getMenuInflater();
		inf.inflate(R.menu.main_action_bar, menu);
		return true;
	}
	
	/** Called when the activity is created. Validates the license */
	@SuppressWarnings("unused")
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(skipLicensing)return;// Licensing is no longer needed
		this.handler=new Handler();
		this.lcc=new MyLicenseCheckerCallback();
		final String uid=Settings.Secure.ANDROID_ID;//why bother with more complicated stuff?
		final AESObfuscator aeso=new AESObfuscator(SALT, getPackageName(), uid);
		final ServerManagedPolicy smp=new ServerManagedPolicy(this, aeso);
		this.lc=new LicenseChecker(this, smp, BASE64_KEY);
		this.lc.checkAccess(this.lcc);
	}
	/** Called when the activity is shown. Initialises the content view for the {@link #currentTarget} */
	@Override
	public void onStart() {
		super.onStart();
		setTitle(Messages.getString("ma.title")); //$NON-NLS-1$
		init(MainActivity.currentTarget);
	}
	@SuppressWarnings("unused")
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(skipLicensing)return;
		this.lc.onDestroy();
	}
	/** Creates and shows the content view
	 * 
	 * @param target the target for the <code>Gene</code>s shown
	 */
	private void init(final long target) {
		MainActivity.spinnerMap.clear();
		final LinearLayout lay=new LinearLayout(this);
		lay.setOrientation(LinearLayout.VERTICAL);
		final TextView txtv=new TextView(this);
		txtv.setText(
				currentTarget==Gene.TARGET_FEMALE?
						Messages.getString("ma.female_traits"):Messages.getString("ma.male_traits")); //$NON-NLS-1$ //$NON-NLS-2$
		txtv.setTypeface(null, Typeface.BOLD);
		lay.addView(txtv);
		for(Entry<String, Set<Gene>> set : Gene.ALL_GENES.entrySet()){
			final String group=set.getKey();
			if(first==false&&!geneSetMap.containsKey(group))
				continue;
			final TextView tv=new TextView(this);
			tv.setText(group);
			final Spinner sp=new Spinner(this);
			final ArrayList<Gene> genesList=new ArrayList<Gene>(set.getValue().size());
			if(first)
				genesList.add(null);
			boolean empty=true;
			for(Gene gene : set.getValue())
				if((gene.targets&target)>0){
					genesList.add(gene);
					empty=false;
				}
			if(empty)
				continue;
			final Gene[] genes=genesList.toArray(new Gene[genesList.size()]);
			sp.setAdapter(new GenesAdapter(this, android.R.layout.simple_spinner_item, genes));
			lay.addView(tv);
			lay.addView(sp);
			MainActivity.spinnerMap.put(group, sp);
		}
		if(Build.VERSION.SDK_INT<11){
			final Button nextBut=new Button(this);
			nextBut.setOnClickListener(new OnClickListener(){
				@Override
				//TODO: fix this kludgeful method
				public void onClick(View v) {
					nextButClicked();
				}
			});
			nextBut.setText(
					MainActivity.currentTarget==Gene.TARGET_FEMALE?
							Messages.getString("ma.next"):Messages.getString("ma.finish")); //$NON-NLS-1$ //$NON-NLS-2$
			lay.addView(nextBut);
		}
		final ScrollView scroll=new ScrollView(this);
		scroll.addView(lay);
		setContentView(scroll);
	}
	/**
	 * Add the selections of each <code>Spinner</code> to the <code>GeneSet</code>s
	 * @return true if the app should continue, false if it shouldn't
	 */
	private boolean saveState() {
		boolean allSkipped=true;
		for(Entry<CharSequence,Spinner> ent : spinnerMap.entrySet()){
			final String group=ent.getKey().toString();
			final Gene theGene=(Gene)ent.getValue().getSelectedItem();
			if(theGene==null)
				continue;
			allSkipped=false;
			if(!MainActivity.geneSetMap.containsKey(group))
				MainActivity.geneSetMap.put(group, new GeneSet(group.toString()));
			geneSetMap.get(group).add(theGene, 1);
		}
		if(allSkipped){
			Toast.makeText(this, Messages.getString("ma.all_skipped"), Toast.LENGTH_SHORT).show(); //$NON-NLS-1$
			return false;
		}
		return true;
	}

	/** Throws an error telling the user that the app isn't licensed */
	private void err(){
		this.handler.post(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(MainActivity.this, Messages.getString("MainActivity.app_not_licensed"), Toast.LENGTH_SHORT).show(); //$NON-NLS-1$
				finish();
			}
		});
	}
	
	/**
	 * Called when user clicks on the nextBut equivalent in the actionbar
	 * @param item the MenuItem [unused]
	 */
	public void nextButClicked(final MenuItem item){
		nextButClicked();
	}
	
	private void nextButClicked(){
		if(first){
			if(!saveState())
				return;
			currentTarget=Gene.TARGET_MALE;
			first=false;
			init(Gene.TARGET_MALE);
		}else{
			if(!saveState())
				return;
			currentTarget=Gene.TARGET_FEMALE;
			first=true;
			ResultActivity.setGeneSetMap(geneSetMap);
			geneSetMap=new HashMap<CharSequence,GeneSet>(30);
			final Intent i=new Intent(this,ResultActivity.class);
			this.startActivity(i);
		}
	}
}