/* Copyright © 2012 Marius Gavrilescu
 *
 * This file is part of Offspring Calculator.
 * Offspring Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Offspring Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Offspring Calculator.  If not, see <http://www.gnu.org/licenses/>.
 */
package cc.co.mgvx.oc;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * An <code>Activity</code> showing the results of a hybridization
 * @author root
 */
public final class ResultActivity extends Activity {
	private static Map<CharSequence,GeneSet> geneSetMap;
	/** The intent for sharing */
	private Intent shareIntent;
	/** The ShareActionProvider, disguised as an Object. Only exists in sdk >= 14 */
	private Object sap=null;
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(Build.VERSION.SDK_INT<11)
			return super.onCreateOptionsMenu(menu);
		final MenuInflater inflater=getMenuInflater();
		inflater.inflate(R.menu.results_action_bar, menu);
		this.sap=null;
		if(Build.VERSION.SDK_INT>=14){
			final MenuItem item=menu.findItem(R.id.share);
			try{
				final Method gap=MenuItem.class.getMethod("getActionProvider", (Class[])null); //$NON-NLS-1$
				this.sap=gap.invoke(item, (Object[])null);
			}catch(Exception ex){
				ex.printStackTrace();
				Toast.makeText(this, "Error 2! Sharing disabled.", Toast.LENGTH_SHORT); //$NON-NLS-1$
			}
			if(this.shareIntent!=null)
				setShareIntent();
		}
		return true;
	}
	
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(Messages.getString("ra.title")); //$NON-NLS-1$
		final LinearLayout lay=new LinearLayout(this);
		final StringBuilder shareSB=new StringBuilder(Messages.getString("ra.title")); //$NON-NLS-1$
		shareSB.append(":\n\n"); //$NON-NLS-1$
		lay.setOrientation(LinearLayout.VERTICAL);
		for(Entry<CharSequence, GeneSet> ent : geneSetMap.entrySet()){
			final TextView head=new TextView(this);
			final String group=(String) ent.getKey();
			final GeneSet gs=ent.getValue();
			head.setText(group);
			shareSB.append(group);
			shareSB.append('\n');
			lay.addView(head);
			gs.hybridize();
			final Map<Gene, Float> ratios=gs.getRatios();
			final Map<String, Float> uniqRatios=new HashMap<String, Float>(ratios.size(), 1);
			for(Entry<Gene, Float> rat : ratios.entrySet()){
				final TextView child=new TextView(this);
				final Gene theGene=rat.getKey();
				final float theFreq=rat.getValue();
				final String desc=theGene.getDescription();
				final String htmlString=String.format("<html>%s: <b>%.2f%%</b></html>",  //$NON-NLS-1$
						theGene.toNonHtmlString(),
						theFreq);
				child.setGravity(Gravity.RIGHT);
				child.setText(Html.fromHtml(htmlString));
				if(uniqRatios.containsKey(desc))
					uniqRatios.put(desc, 
							uniqRatios.get(desc)+theFreq);
				else
					uniqRatios.put(desc, theFreq);
				lay.addView(child);
			}
			for(Entry<String, Float> pair : uniqRatios.entrySet())
				shareSB.append(String.format("    %.2f%% %s\n",  //$NON-NLS-1$
						pair.getValue(),
						pair.getKey()));
			shareSB.append('\n');
		}
		if(Build.VERSION.SDK_INT<11){
			final Button shBut=new Button(this);
			shBut.setText(Messages.getString("ra.share")); //$NON-NLS-1$
			shBut.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					share(null);
				}
			});
			lay.addView(shBut);
		}
		final ScrollView sv=new ScrollView(this);
		sv.addView(lay);
		setContentView(sv);
		this.shareIntent=new Intent(Intent.ACTION_SEND);
		this.shareIntent.putExtra(Intent.EXTRA_TEXT,shareSB.toString());
		this.shareIntent.putExtra(Intent.EXTRA_SUBJECT, Messages.getString("ra.title")); //$NON-NLS-1$
		this.shareIntent.setType("text/plain"); //$NON-NLS-1$
		if(this.sap!=null)
			setShareIntent();
	}
	/**
	 * Utility function to set the shareIntent of the ShareActionProvider via reflection
	 * It is a no-op for api level < 14
	 */
	private void setShareIntent(){
		if(Build.VERSION.SDK_INT<14)
			return;
		try{
			final Class<?> sapClass=Class.forName("android.widget.ShareActionProvider"); //$NON-NLS-1$
			final Method setSI=sapClass.getMethod("setShareIntent",Intent.class); //$NON-NLS-1$
			setSI.invoke(this.sap, this.shareIntent);
		}catch(Exception ex){
			ex.printStackTrace();
			Toast.makeText(this, "Error 1! Sharing disabled.", Toast.LENGTH_SHORT); //$NON-NLS-1$
		}
	}
	/** Sets {@link #geneSetMap} to another map
	 * 
	 * @param geneSetMap the new map
	 */
	public static void setGeneSetMap(Map<CharSequence,GeneSet> geneSetMap){
		ResultActivity.geneSetMap=geneSetMap;	
	}
	/**
	 * Shares the results by running shareIntent
	 * @param ignored not used
	 */
	public void share(final MenuItem ignored){
		startActivity(Intent.createChooser(this.shareIntent, Messages.getString("ra.share"))); //$NON-NLS-1$
	}
}
