/* Copyright © 2012 Marius Gavrilescu
 *
 * This file is part of Offspring Calculator.
 * Offspring Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Offspring Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Offspring Calculator.  If not, see <http://www.gnu.org/licenses/>.
 */
package cc.co.mgvx.oc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Represents a pair of two alleles.
 *
 * A <code>Gene</code> has four fields:
 * <ul>
 * <li> <code>left</code> -- the left allele
 * <li> <code>right</code> -- the right allele
 * <li> <code>targets</code> -- the creatures this gene applies to
 * <li> <code>group</code> -- the trait influenced by this <code>Gene</code>. <code>Gene</code>s with identical <code>group</code>s are placed in the same selector
 * </ul>
 * @author Marius Gavrilescu
 */
public final class Gene implements Comparable<Gene> {
    /** Map from a <code>Gene</code> to its description */
    private static final Map<Gene, String> LUT=new HashMap<Gene, String>(
	    100,1);
    /** Map from a <code>group</code> to all the <code>Gene</code>s in that group */
    public static final Map<String,Set<Gene>> ALL_GENES=new HashMap<String,Set<Gene>>(100);
    /** The <code>Gene</code> applies to males. Can be combined with other <code>TARGET_*</code> constants
     * @see #TARGET_FEMALE
     */
    public static final long TARGET_MALE=1;
    /** The <code>Gene</code> applies to females. Can be combined with other <code>TARGET_*</code> constants
     * @see #TARGET_MALE
     */
    public static final long TARGET_FEMALE=2;

    //<editor-fold defaultstate="collapsed" desc="initializer block for LUT">
    static{
    	final ResourceBundle bundle=ResourceBundle.getBundle("cc.co.mgvx.oc.gene_bundle"); //$NON-NLS-1$
	LUT.put(new Gene(
		"P<sub>1</sub>P<sub>2</sub>", //$NON-NLS-1$
		"P<sub>1</sub>P<sub>2</sub>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("skin")), //$NON-NLS-1$
		bundle.getString("skin.dark_brown")); //$NON-NLS-1$
	LUT.put(new Gene(
		"P<sub>1</sub>p<sub>2</sub>", //$NON-NLS-1$
		"P<sub>1</sub>p<sub>2</sub>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("skin")), //$NON-NLS-1$
		bundle.getString("skin.brown")); //$NON-NLS-1$
	LUT.put(new Gene(
		"P<sub>1</sub>p<sub>2</sub>", //$NON-NLS-1$
		"p<sub>1</sub>p<sub>2</sub>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("skin")), //$NON-NLS-1$
		bundle.getString("skin.light_brown")); //$NON-NLS-1$
	LUT.put(new Gene(
		"p<sub>1</sub>p<sub>2</sub>", //$NON-NLS-1$
		"p<sub>1</sub>p<sub>2</sub>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("skin")), //$NON-NLS-1$
		bundle.getString("skin.white")); //$NON-NLS-1$

	LUT.put(new Gene(
		"E<sup>br</sup>", //$NON-NLS-1$
		"E<sup>br</sup>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("eyes")), bundle.getString("eyes.brown")); //$NON-NLS-1$ //$NON-NLS-2$
	LUT.put(new Gene(
		"E<sup>br</sup>", //$NON-NLS-1$
		"E<sup>gr</sup>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("eyes")), bundle.getString("eyes.hazel")); //$NON-NLS-1$ //$NON-NLS-2$
	LUT.put(new Gene(
		"E<sup>gr</sup>", //$NON-NLS-1$
		"E<sup>gr</sup>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("eyes")), bundle.getString("eyes.green")); //$NON-NLS-1$ //$NON-NLS-2$
	LUT.put(new Gene(
		"E<sup>gr</sup>", //$NON-NLS-1$
		"E<sup>bl</sup>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("eyes")), bundle.getString("eyes.blue_green")); //$NON-NLS-1$ //$NON-NLS-2$
	LUT.put(new Gene(
		"E<sup>bl</sup>", //$NON-NLS-1$
		"E<sup>bl</sup>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("eyes")), bundle.getString("eyes.blue")); //$NON-NLS-1$ //$NON-NLS-2$
	LUT.put(new Gene(
		"E<sup>bl</sup>", //$NON-NLS-1$
		"E<sup>br</sup>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("eyes")), bundle.getString("eyes.dark_brown")); //$NON-NLS-1$ //$NON-NLS-2$

	LUT.put(new Gene("B", "B", TARGET_MALE|TARGET_FEMALE, bundle.getString("face")), bundle.getString("face.round")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("B", "b", TARGET_MALE|TARGET_FEMALE, bundle.getString("face")), bundle.getString("face.round")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("b", "b", TARGET_MALE|TARGET_FEMALE, bundle.getString("face")), bundle.getString("face.long")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	LUT.put(new Gene("N", "N", TARGET_MALE|TARGET_FEMALE, bundle.getString("nose")), bundle.getString("nose.long")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("N", "n", TARGET_MALE|TARGET_FEMALE, bundle.getString("nose")), bundle.getString("nose.long")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("n", "n", TARGET_MALE|TARGET_FEMALE, bundle.getString("nose")), bundle.getString("nose.short")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	LUT.put(new Gene("P", "P", TARGET_MALE|TARGET_FEMALE, bundle.getString("freckles")), bundle.getString("freckles.yes")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("P", "p", TARGET_MALE|TARGET_FEMALE, bundle.getString("freckles")), bundle.getString("freckles.yes")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("p", "p", TARGET_MALE|TARGET_FEMALE, bundle.getString("freckles")), bundle.getString("freckles.no")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	LUT.put(new Gene("H", "H", TARGET_MALE|TARGET_FEMALE, bundle.getString("hair")), bundle.getString("hair.curly")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("H", "h", TARGET_MALE|TARGET_FEMALE, bundle.getString("hair")), bundle.getString("hair.wavy")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("h", "h", TARGET_MALE|TARGET_FEMALE, bundle.getString("hair")), bundle.getString("hair.straight")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	LUT.put(new Gene("D", "D", TARGET_MALE|TARGET_FEMALE, bundle.getString("hair_color")), bundle.getString("hair_color.black")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("D", "d", TARGET_MALE|TARGET_FEMALE, bundle.getString("hair_color")), bundle.getString("hair_color.brown")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("d", "d", TARGET_MALE|TARGET_FEMALE, bundle.getString("hair_color")), bundle.getString("hair_color.blond")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	/*LUT.put(new Gene("V", "V", TARGET_MALE, "Voice type"), "bass");
	LUT.put(new Gene("V", "v", TARGET_MALE, "Voice type"), "bariton");
	LUT.put(new Gene("v", "v", TARGET_MALE, "Voice type"), "tenor");

	LUT.put(new Gene("V", "V", TARGET_FEMALE, "Voice type"), "soprano");
	LUT.put(new Gene("V", "v", TARGET_FEMALE, "Voice type"), "mezzo-soprano");
	LUT.put(new Gene("v", "v", TARGET_FEMALE, "Voice type"), "altist");*/

	LUT.put(new Gene("M", "M", TARGET_MALE|TARGET_FEMALE, bundle.getString("hand")), bundle.getString("hand.right")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("M", "m", TARGET_MALE|TARGET_FEMALE, bundle.getString("hand")), bundle.getString("hand.right")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("m", "m", TARGET_MALE|TARGET_FEMALE, bundle.getString("hand")), bundle.getString("hand.left")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	LUT.put(new Gene("S", "S", TARGET_MALE|TARGET_FEMALE, bundle.getString("syndact")), bundle.getString("syndact.yes")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("S", "s", TARGET_MALE|TARGET_FEMALE, bundle.getString("syndact")), bundle.getString("syndact.yes")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("s", "s", TARGET_MALE|TARGET_FEMALE, bundle.getString("syndact")), bundle.getString("syndact.no")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	LUT.put(new Gene("O", "O", TARGET_MALE|TARGET_FEMALE, bundle.getString("brachydact")), bundle.getString("brachydact.yes")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("O", "o", TARGET_MALE|TARGET_FEMALE, bundle.getString("brachydact")), bundle.getString("brachydact.yes")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("o", "o", TARGET_MALE|TARGET_FEMALE, bundle.getString("brachydact")), bundle.getString("brachydact.no")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	LUT.put(new Gene("HbA", "HbA", TARGET_MALE|TARGET_FEMALE, bundle.getString("sickle")), bundle.getString("sickle.no")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("HbA", "HbS", TARGET_MALE|TARGET_FEMALE, bundle.getString("sickle")), bundle.getString("sickle.yes")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	LUT.put(new Gene("HbS", "HbS", 0, bundle.getString("sickle")), bundle.getString("sickle.dead")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	LUT.put(new Gene(
		"T<sup>M</sup>", //$NON-NLS-1$
		"T<sup>M</sup>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("talasemy")), //$NON-NLS-1$
		bundle.getString("talasemy.major")); //$NON-NLS-1$
	LUT.put(new Gene(
		"T<sup>M</sup>", //$NON-NLS-1$
		"T<sup>N</sup>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("talasemy")), //$NON-NLS-1$
		bundle.getString("talasemy.minor")); //$NON-NLS-1$
	LUT.put(new Gene(
		"T<sup>N</sup>", //$NON-NLS-1$
		"T<sup>N</sup>", //$NON-NLS-1$
		TARGET_MALE|TARGET_FEMALE,
		bundle.getString("talasemy")), //$NON-NLS-1$
		bundle.getString("talasemy.none")); //$NON-NLS-1$

	LUT.put(new Gene(
		"X<sup>m</sup>", //$NON-NLS-1$
		"Y", //$NON-NLS-1$
		TARGET_MALE,
		bundle.getString("duchenne")), //$NON-NLS-1$
		bundle.getString("duchenne.yes")); //$NON-NLS-1$
	LUT.put(new Gene(
		"X", //$NON-NLS-1$
		"Y", //$NON-NLS-1$
		TARGET_MALE,
		bundle.getString("duchenne")), //$NON-NLS-1$
		bundle.getString("duchenne.no")); //$NON-NLS-1$
	LUT.put(new Gene(
		"X<sup>m</sup>", //$NON-NLS-1$
		"X", //$NON-NLS-1$
		TARGET_FEMALE,
		bundle.getString("duchenne")), //$NON-NLS-1$
		bundle.getString("duchenne.carrier")); //$NON-NLS-1$
	LUT.put(new Gene(
		"X<sup>m</sup>", //$NON-NLS-1$
		"X<sup>m</sup>", //$NON-NLS-1$
		TARGET_FEMALE,
		bundle.getString("duchenne")), //$NON-NLS-1$
		bundle.getString("duchenne.yes")); //$NON-NLS-1$
	LUT.put(new Gene(
		"X", //$NON-NLS-1$
		"X", //$NON-NLS-1$
		TARGET_FEMALE,
		bundle.getString("duchenne")), //$NON-NLS-1$
		bundle.getString("duchenne.no")); //$NON-NLS-1$

	LUT.put(new Gene(
		"X<sup>d</sup>", //$NON-NLS-1$
		"Y", //$NON-NLS-1$
		TARGET_MALE,
		bundle.getString("colorblind")), bundle.getString("colorblind.yes")); //$NON-NLS-1$ //$NON-NLS-2$
	LUT.put(new Gene(
		"X", //$NON-NLS-1$
		"Y", //$NON-NLS-1$
		TARGET_MALE,
		bundle.getString("colorblind")), bundle.getString("colorblind.no")); //$NON-NLS-1$ //$NON-NLS-2$

	LUT.put(new Gene(
		"X<sup>d</sup>", //$NON-NLS-1$
		"X<sup>d</sup>", //$NON-NLS-1$
		TARGET_FEMALE,
		bundle.getString("colorblind")), //$NON-NLS-1$
		bundle.getString("colorblind.yes")); //$NON-NLS-1$
	LUT.put(new Gene(
		"X<sup>d</sup>", //$NON-NLS-1$
		"X", //$NON-NLS-1$
		TARGET_FEMALE,
		bundle.getString("colorblind")), //$NON-NLS-1$
		bundle.getString("colorblind.carrier")); //$NON-NLS-1$
	LUT.put(new Gene(
		"X", //$NON-NLS-1$
		"X", //$NON-NLS-1$
		TARGET_FEMALE,
		bundle.getString("colorblind")), //$NON-NLS-1$
		bundle.getString("colorblind.no")); //$NON-NLS-1$

	LUT.put(new Gene(
		"X<sup>h</sup>", //$NON-NLS-1$
		"Y", //$NON-NLS-1$
		TARGET_MALE,
		bundle.getString("hemo")), bundle.getString("hemo.yes")); //$NON-NLS-1$ //$NON-NLS-2$
	LUT.put(new Gene(
		"X", //$NON-NLS-1$
		"Y", //$NON-NLS-1$
		TARGET_MALE,
		bundle.getString("hemo")), bundle.getString("hemo.no")); //$NON-NLS-1$ //$NON-NLS-2$

	LUT.put(new Gene(
		"X<sup>h</sup>", //$NON-NLS-1$
		"X<sup>h</sup>", //$NON-NLS-1$
		TARGET_FEMALE,
		bundle.getString("hemo")), //$NON-NLS-1$
		bundle.getString("hemo.yes")); //$NON-NLS-1$
	LUT.put(new Gene(
		"X<sup>h</sup>", //$NON-NLS-1$
		"X", //$NON-NLS-1$
		TARGET_FEMALE,
		bundle.getString("hemo")), //$NON-NLS-1$
		bundle.getString("hemo.carrier")); //$NON-NLS-1$
	LUT.put(new Gene(
		"X", //$NON-NLS-1$
		"X", //$NON-NLS-1$
		TARGET_FEMALE,
		bundle.getString("hemo")), //$NON-NLS-1$
		bundle.getString("hemo.no")); //$NON-NLS-1$
    }
    //</editor-fold>

    /** The left part of the <code>Gene</code> */
    public final String left;
    /** The right part of the <code>Gene</code> */
    public final String right;
    /** The creatures this <code>Gene</code> applies to */
    public final long targets;
    /** The group of this gene */
    public final String group;

    /** Default constructor for <code>Gene</code>
     *
     * @param left The left part of the <code>Gene</code>
     * @param right The right part of the <code>Gene</code>
     * @param targets the targets of this <code>Gene</code>
     * @param group the group of this <code>Gene</code>
     * @param addToPool If true, the <code>Gene</code> is added to {@link #ALL_GENES}
     */
    public Gene(final String left, final String right, final long targets, final String group,final boolean addToPool){
	final String lft=left.intern();
	final String rht=right.intern();
	if(lft.compareTo(rht)<=0){
	    this.left = lft;
	    this.right = rht;
	}else{
	    this.left = rht;
	    this.right = lft;
	}
	this.targets = targets;
	this.group = group;
	if(!addToPool)
	    return;
	if(ALL_GENES.containsKey(group))
	    ALL_GENES.get(group).add(this);
	else{
	    final HashSet<Gene> genes=new HashSet<Gene>(5,1);
	    genes.add(this);
	    ALL_GENES.put(group, genes);
	}
    }
    /**
     * Internal-only constructor. Does the same thing as <code>Gene(left,right,targets,group,true)</code>
     * @param left the left part
     * @param right the right part
     * @param targets the targets
     * @param group the group
     * @see #Gene(java.lang.String, java.lang.String, long, java.lang.String, boolean)
     */
    private Gene(final String left, final String right,final long targets,final String group) {
	this(left, right, targets, group, true);
    }

    /**
     * Converts the <code>Gene</code> into a human-friendly <code>String</code>
     * The result contains the representation of the <code>Gene</code> and its description
     * @return the <code>String</code>
     * @see #getDescription()
     */
    public String toNonHtmlString(){
	return this.left+this.right+" -- "+getDescription(); //$NON-NLS-1$
    }
    /**
     * Wraps the result of {@link #toNonHtmlString()} in "<html>" and "</html>" tags
     * @return the HTML-ized string form of this <code>Gene</code>
     */
    @Override
    public String toString() {
	return "<html>"+toNonHtmlString()+"</html>"; //$NON-NLS-1$ //$NON-NLS-2$
    }

    /**
     * Checks whether this <code>Gene</code> is equal to <code>obj</code>. Two <code>Gene</code>s are equal iff both their parts are identical
     * @param obj the object to	compare with
     * @return true if this <code>Gene</code> equals <code>obj</code>
     */
    @Override
    public boolean equals(final Object obj) {
	if(!(obj instanceof Gene))
	    return false;
	final Gene gene=(Gene)obj;
	return this.left==gene.left&&this.right==gene.right&&this.group==gene.group;
    }

    /**
     * Returns the hash code value of this <code>Gene</code>
     * @return the hash code value
     */
    @Override
    public int hashCode() {
	return (this.left+'-'+this.right).hashCode();
    }

    /**
     * Checks whether this <code>Gene</code> is less than, equal or greater than <code>gene</code>.
     * It does this using <code>compareTo</code> on the {@link #toString()} values of the <code>Gene</code>s
     * @param gene the <code>Gene</code> to compare with
     * @return <code>-1</code> if this is less than <code>gene</code>, 0 if they are equal and 1 otherwise
     */
    @Override
    public int compareTo(final Gene gene) {
	return toString().compareTo(gene.toString());
    }

    /**
     * Returns the human-friendly description of this <code>Gene</code>
     * @return the description
     * @see #LUT
     */
    public String getDescription(){
	return LUT.get(this);
    }
}